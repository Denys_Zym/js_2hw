// Теория
//1
// Типы данных: string, number, bigint (большие числа ±(253-1)), boolen (true/false), object (более сложные структуры, например функции), null (отдельный тип, но показывается как объект), undefined (показывает одно значение, это неприсвоенное значение переменной), symbol (уникальные идентифмикаторы)
//2
//Равенство == и строгое равенство === (напирмер числа свраниваются только с числами)
//3
//Оператор это арифметическое действие оно выполняется с операндами (значениями переменных)

// Практика
let userName = prompt("Please add your name");
let age = Number(prompt("Please add your age"));
console.log(age);

while (!userName) {
  userName = prompt("Invalid name", userName);
}
while (isNaN(age)) {
  age = prompt("Invalid age", age);
}

if (age < 18) {
  alert("You are not allowed to visit this website!");
} else if (age > 18 && age < 22) {
  let result = confirm("Are you sure you want to continue?");
  result
    ? alert(`Welcome ${userName}`)
    : alert("You are not allowed to visit this website");
} else {
  alert(`Welcome ${userName}`);
}
